const mysql = require("mysql2");
const express = require("express");
const bodyParser = require("body-parser");
const multer  = require("multer");
var archiver = require('archiver');
const fs = require('fs');
const path = require('path');
var archiver = require('archiver');
var p = require('path');

 
const app = express();
const urlencodedParser = bodyParser.urlencoded({extended: false});
let itemsdir;

let time = new Date; 

const pool = mysql.createPool({
  
  host: "localhost", 
  user: "root",
  database: "testdb",

});
 
app.set("view engine", "hbs");
 
// получение списка пользователей
app.get("/", function(req, res){
    pool.query("SELECT * FROM experiment", function(err, data)
	{
		const directoryPath = path.join(__dirname, 'Uploads');
			fs.readdir(directoryPath, function(err, items) { 	
			itemsdir=items;
			});
		pool.query("SELECT * FROM expirementgroup", function(err, data2)
		{
      if(err) return console.log(err);
      res.render("index.hbs", {
          exp: data, group: data2, item: itemsdir
		});
		});
    });
});
app.post("/create", urlencodedParser, function (req, res) {     
    if(!req.body) return res.sendStatus(400);
    const name = req.body.name;
    const description = req.body.description;
    pool.query("INSERT INTO experiment (name, description	) VALUES (?,?)", [name, description], function(err, data) {	  
		fs.mkdir(__dirname+"/uploads/exp_"+data.insertId ,{recursive:true}, (err) => {if (err) throw err;})
		let timenow = new Date; 
		pool.query("INSERT INTO Logs (tablename, id,action,time	) VALUES (?,?,?,?)", [name , data.insertId , "CREATE", timenow]);	
		if(err) return console.log(err);
		res.redirect("/");
		});
});	 
app.post("/edit", urlencodedParser, function (req, res) {      
  if(!req.body) return res.sendStatus(400);
  const name = req.body.name;
  const description = req.body.description;
  const id = req.body.id;  
  pool.query("UPDATE experiment SET name=?, description=? WHERE id=?", [name, description, id], function(err, data) {
    if(err) return console.log(err);
	let timenow = new Date; 	
	pool.query("INSERT INTO Logs (tablename, id,action,time	) VALUES (?,?,?,?)", [name , id , "UPDATE", timenow]);	
    res.redirect("/");
  });
});
app.post("/delete/:id", function(req, res){				
	const id = req.params.id;
	let timenow = new Date; 	
	pool.query("SELECT * FROM experiment WHERE id=?", [id], function(err, data) {		
			pool.query("INSERT INTO Logs (tablename, id,action,time	) VALUES (?,?,?,?)", [data[0].name , id , "DELETE", timenow]);		
		});
	pool.query("DELETE FROM experiment WHERE id=?", [id], function(err, data) {	
    if(err) return console.log(err);
    res.redirect("/");
  });
});
// возвращаем форму для добавления данных
app.get("/create", function(req, res){
    res.render("create.hbs");
});

app.get("/creategrp", function(req, res){
    res.render("creategrp.hbs");
});
// получаем отправленные данные и добавляем их в БД 

 
 
 app.post("/creategrp", urlencodedParser, function (req, res) {
         
    if(!req.body) return res.sendStatus(400);
    const name = req.body.name;
    const description = req.body.description;
	const expid = req.body.exp_id;
    pool.query("INSERT INTO expirementgroup (exp_id,name, description	) VALUES (?,?,?)", [expid, name, description], function(err, data) {	
	fs.mkdir(__dirname+"/uploads/exp_"+expid+"/grp_"+data.insertId ,{recursive:true}, (err) => {if (err) throw err;})
		
	let timenow = new Date; 	
	pool.query("INSERT INTO Logs (tablename, id,action,time	) VALUES (?,?,?,?)", [name , data.insertId , "INSERT", timenow]);	
	
      if(err) return console.log(err);
      res.redirect("/");
    });
	
});

// получем id редактируемого пользователя, получаем его из бд и отправлям с формой редактирования
app.get("/edit/:id", function(req, res){
  const id = req.params.id;
  pool.query("SELECT * FROM experiment WHERE id=?", [id], function(err, data) {
    if(err) return console.log(err);
     res.render("edit.hbs", {
        user: data[0]
    });
  });
});

app.get("/editgrp/:id", function(req, res){
  const id = req.params.id;
  pool.query("SELECT * FROM expirementgroup WHERE id=?", [id], function(err, data) {
    if(err) return console.log(err);
     res.render("editgrp.hbs", {
        user: data[0]
    });
  });
});
// получаем отредактированные данные и отправляем их в БД


app.post("/editgrp", urlencodedParser, function (req, res) {
        
  if(!req.body) return res.sendStatus(400);
  const name = req.body.name;
  const description = req.body.description;
  const id = req.body.id;
  const expid = req.body.exp_id; 
  
  pool.query("UPDATE expirementgroup SET name=?, description=?, exp_id=? WHERE id=?", [name, description, expid, id], function(err, data) {
	
	let timenow = new Date; 
	pool.query("INSERT INTO Logs (tablename, id,action,time	) VALUES (?,?,?,?)", [name , id , "UPDATE", timenow]);	
	
    if(err) return console.log(err);
	
	
    res.redirect("/");
	

  });
});
 
// получаем id удаляемого пользователя и удаляем его из бд


app.post("/deletegrp/:id", function(req, res){
         
	const id = req.params.id;	
	let timenow = new Date; 	
		pool.query("SELECT * FROM expirementgroup WHERE id=?", [id], function(err, data) {		
			pool.query("INSERT INTO Logs (tablename, id,action,time	) VALUES (?,?,?,?)", [data[0].name , id , "DELETE", timenow]);		
	});
	
	pool.query("DELETE FROM expirementgroup WHERE id=?", [id], function(err, data) {
		if(err) return console.log(err);
		
		//let timenow = new Date; 
		res.redirect("/");
	
	});
});

//файлы

let pathfile;
//app.use(express.static(__dirname));
	app.post('/path', urlencodedParser,function(req,res){
	pathfile=__dirname+"/uploads/exp_"+req.body.exp+'/grp_'+req.body.grp;
	
})
//app.use(multer({storage:storageConfig}).single("filedata"));
const storageConfig = multer.diskStorage({
    destination: (req, file, cb) =>{
        cb(null, pathfile);
    },
    filename: (req, file, cb) =>{
        cb(null, file.originalname);
    }
});

app.use(express.static(__dirname));
app.use(multer({storage:storageConfig}).single("filedata"));

app.post("/upload", function (req, res, next) {
    let filedata = req.file;
    if(!filedata)
        res.send("Ошибка при загрузке файла");
    else
        res.send("Файл загружен");
});
	
	//ls
	
let id;	
app.post('/files/:id', urlencodedParser,function(req,res){
	id = req.params.id;
	const directoryPath = path.join(__dirname, 'Uploads','exp_'+id);
			fs.readdir(directoryPath, function(err, items) { 
					res.render("files.hbs",{
					  item: items
					});
					
					
			});
	 });
let imagepath;
app.post('/expfold/:item', urlencodedParser,function(req,res){
	console.log(1);
	let item = req.params.item;
	imagepath = path.join(__dirname, 'Uploads','exp_'+id,item);
	console.log(imagepath);
			fs.readdir(imagepath, function(err, items) { 
					res.render("files2.hbs",{
					  item: items
					});
			});
	 });
app.post('/opnimg/:item', urlencodedParser,function(req,res){
	
	let item = req.params.item;
	let fullimagepath = path.join(imagepath,item);
	res.statusCode = 200;
    res.setHeader("Content-Type", "image/jpeg");
	require("fs").readFile(fullimagepath, (err, image) => {
      res.end(image);
	  
    });
});

app.post('/loadarhive', urlencodedParser,function(req,res){
	 var archive = archiver('zip');

	  archive.on('error', function(err) {
		res.status(500).send({error: err.message});
	  });
	  archive.on('end', function() {
		console.log('Archive wrote %d bytes', archive.pointer());
	  });
	  res.attachment('archive-name.zip');

	  archive.pipe(res);

	  var files = fs.readdirSync(imagepath);
	
	  for(var i in files) {
		archive.file(files[i], { name: p.basename(files[i]) });
	  }
		console.log(files);
	  var directories = [imagepath];
		console.log(directories);
	  for(var i in directories) {
		archive.directory(directories[i], directories[i].replace(imagepath, ''));
	  }

	  archive.finalize();  
});




let ldap_url = 'ldap://ipa1-hlit.jinr.ru:389';
let ldap_realm = 'HYBRILIT.JINR.RU';
let ldap_base = 'cn=accounts,dc=hybrilit,dc=jinr,dc=ru';
let ldap_users = 'cn=users,'+ ldap_base;
let ldap_groups = 'cn=groups,' + ldap_base;
let ldap_adming_group = 'hlit-web-admin';
let ldap_chief_group = 'hlit-web-chief';
let cors_allowed = ['*'];
let jwt_secret = "secret";
let ldap = require('ldapjs');
let ldap2date = require('ldap2date');

app.post('/login', function (req, res) {
    let username = req.params.username;
	console.log(req.params.username);
    let dn = 'uid=' + username + ',' + ldap_users;
    let password = req.params.password;
    let ldapclient = ldap.createClient({ url: ldap_url });
    ldapclient.bind(dn, password, function (err) {
        if (err) {
            res.send(err);
        } else {
            let opts = {};
            ldapclient.search(dn, opts, function (err, ldapres) {
                ldapres.on('searchEntry', function (entry) {
                    // console.log('entry: ' + JSON.stringify(entry.object));
                    let obj = entry.object;

                    let isAdmin = false;
                    let isChief = false;

                    // filtring groups
                    let group_members = [];
                    if (obj.memberOf.constructor === Array)
                        group_members = obj.memberOf
                    else
                        group_members.push(obj.memberOf);
                    let groups = group_members.filter(function (arrayElement) {
                        return arrayElement.indexOf(ldap_base) == arrayElement.length - ldap_base.length;
                    });

                    // removing useless strings
                    for (let i = 0; i < groups.length; i++) {
                        groups[i] = groups[i].replace(',' + ldap_groups, '');
                        groups[i] = groups[i].replace("cn=", '');
                        if (groups[i] === ldap_adming_group) isAdmin = true;
                        if (groups[i] === ldap_chief_group) isChief = true;
                    }

                    if (obj.krbLastSuccessfulAuth)
                        obj.krbLastSuccessfulAuth = ldap2date.parse(obj.krbLastSuccessfulAuth)

                    if (obj.krbLastFailedAuth)
                        obj.krbLastFailedAuth = ldap2date.parse(obj.krbLastFailedAuth)
                    // creating profile
                    let profile = {
                        dn: obj.dn,
                        ipaUniqueID: obj.ipaUniqueID,
                        krbPrincipalName: obj.krbPrincipalName,
                        uid: obj.uid,
                        displayName: obj.displayName,
                        mobile: obj.mobile,
                        mail: obj.mail,
                        uidNumber: obj.uidNumber,
                        gidNumber: obj.gidNumber,
                        groups: groups,
                        loginShell: obj.loginShell,
                        homeDirectory: obj.homeDirectory,
                        krbPasswordExpiration: ldap2date.parse(obj.krbPasswordExpiration),
                        krbLastPwdChange: ldap2date.parse(obj.krbLastPwdChange),
                        krbExtraData: obj.krbExtraData,
                        krbLastSuccessfulAuth: obj.krbLastSuccessfulAuth,
                        krbLastFailedAuth: obj.krbLastFailedAuth,
                        krbLoginFailedCount: obj.krbLoginFailedCount,
                        isAdmin: isAdmin,
                        isChief: isChief
                    };
                    res.send({
                        id_token: jwt.sign(profile, jwt_secret, { expiresIn: "1 day" })
                    });
                });
                ldapres.on('searchReference', function (referral) {
                    console.log('referral: ' + referral.uris.join());
                    ldapclient.unbind();
                    res.send(referral.uris.join());
                });
                ldapres.on('error', function (err) {
                    console.error('error: ' + err.message);
                    ldapclient.unbind();
                    res.send(err);
                });
                ldapres.on('end', function (result) {
                    console.log('ldap: User "' + username + '" login with status ' + result.status);
                    ldapclient.unbind();
                });
            });
        }
    });
});



app.listen(3000, function(){
  console.log("Сервер ожидает подключения...");
});